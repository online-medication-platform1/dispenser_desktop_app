package com.ds2020.UI.Controller;

import com.ds2020.RPCMethods;
import com.ds2020.UI.Util.JTableButtonModel;
import com.ds2020.UI.View.IDispenserView;
import com.ds2020.rmi.models.MedicationSimpleClass;


import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

import static java.lang.Thread.sleep;

public class DispenserController{

    private final IDispenserView dispenserView;
    private final RPCMethods service;
    private List<MedicationSimpleClass> medicationSimpleClassList;
    //private String patientID = "9ca84a6a-46d1-4224-8307-f00a0958842e";
    private String patientID = "598a03af-3af6-44de-a7e0-4c68e856a3bd";

    public DispenserController(IDispenserView iDispenserView, RPCMethods service){
        dispenserView = iDispenserView;
        this.service = service;
        this.displayCurrentTime();


        this.medicationSimpleClassList = service.getMedicationPlan(patientID);
        for(MedicationSimpleClass med : medicationSimpleClassList)
			System.out.println(med.getName());

        this.updateMedicationPlan();
        //this.medicationNotTaken();

        dispenserView.showMedicationTable(this.retrieveProperties(medicationSimpleClassList));
    }

    public void takePill(int row){
        Date date = new Date(System.currentTimeMillis());

        MedicationSimpleClass med = medicationSimpleClassList.get(row);
        String[] interval = med.getTimeInterval().split("-");
        Integer minHTake = Integer.parseInt(interval[0]);
        Integer maxHTake = Integer.parseInt(interval[1]);
        if(minHTake<=date.getHours() && maxHTake>date.getHours()){
            medicationSimpleClassList.remove(row);
            service.medicationTaken(patientID, med.getName());
         }
    }



    public <T> JTableButtonModel retrieveProperties(java.util.List<T> lista) {

        DefaultTableModel model = new DefaultTableModel();

        int i=0;
        int j=0;
        int ok=1;
        Object[][] rows = new Object[lista.size()][4];
        String[] columns = new String[4];
        int noColumns = 0;

        for(Object object:lista) {

            for(Field field: getAllFields(new LinkedList<>(), object.getClass()) ) {
                field.setAccessible(true);
                Object value;
                try {
                    value=field.get(object);
                    if(ok==1 && !field.getName().equals("id"))
                        columns[i] = field.getName();
                    if(!field.getName().equals("id") && value!=null) {
                        rows[j][i] = value.toString();

                        i++;
                    }
                }
                catch(IllegalArgumentException | IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
            //model.addRow(data);
            noColumns = i;
            i=0;
            j++;
            ok=0;
        }

        int noRows = j + 1;
        columns[noColumns] = "";

        for(int k=0; k<rows.length; k++){
            JButton taken = new JButton("Taken");
            taken.setFont(new Font("Arial", Font.PLAIN, 30));
            rows[k][noColumns] = taken;
        }
        JTableButtonModel medicationTable = new JTableButtonModel(rows,columns);

        return medicationTable;
    }

    public static List<Field> getAllFields(List<Field> fields, Class<?> type) {
        fields.addAll(Arrays.asList(type.getDeclaredFields()));

        if (type.getSuperclass() != null) {
            getAllFields(fields, type.getSuperclass());
        }
        return fields;
    }

    public void displayCurrentTime(){
        Thread clock = new Thread(() -> {
            SimpleDateFormat formatter= new SimpleDateFormat("HH:mm:ss");
            try {
                while(true) {
                    Date date = new Date(System.currentTimeMillis());
                    dispenserView.showCurrentTime(formatter.format(date));
                    sleep(1000);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        clock.start();
    }

    public void updateMedicationPlan(){
        Thread clock = new Thread(() -> {
            SimpleDateFormat formatter= new SimpleDateFormat("HH:mm");
            try {
                while(true) {
                    Date date = new Date(System.currentTimeMillis());
                    if(formatter.format(date).equals("00:00")){
                        this.medicationSimpleClassList = service.getMedicationPlan(patientID);
                        dispenserView.showMedicationTable(this.retrieveProperties(medicationSimpleClassList));
                    }
                    sleep(1000*60*60);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        clock.start();
    }

    public void medicationNotTaken(){
        Thread clock = new Thread(() -> {
            try {
                while(true) {
                    Date date = new Date(System.currentTimeMillis());
                    List<MedicationSimpleClass> medList = new ArrayList<>();

                    for(MedicationSimpleClass med: this.medicationSimpleClassList){
                        String[] interval = med.getTimeInterval().split("-");
                        //Integer minHTake = Integer.parseInt(interval[0]);
                        Integer maxHTake = Integer.parseInt(interval[1]);
                        if(maxHTake<=date.getHours()){
                            service.medicationNotTaken(patientID, med.getName());
                            //md = med;
                            medList.add(med);
                        }
                    }
                    for(MedicationSimpleClass med: medList){
                        this.medicationSimpleClassList.remove(med);
                        dispenserView.showMedicationTable(this.retrieveProperties(medicationSimpleClassList));
                    }


                    sleep(1000*60*60);
                    //sleep(1000*60*60);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        clock.start();
    }

    public void valueTableSelected(){
        int col = dispenserView.getSelectedColumn();
        int row = dispenserView.getSelectedRow();
        if(col == 3){
            this.takePill(row);
            dispenserView.showMedicationTable(this.retrieveProperties(medicationSimpleClassList));
        }
    }

}
