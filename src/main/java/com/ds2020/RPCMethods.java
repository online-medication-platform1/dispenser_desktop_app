package com.ds2020;

import com.ds2020.rmi.models.MedicationSimpleClass;
import java.util.List;

public interface RPCMethods {
    List<MedicationSimpleClass> getMedicationPlan(String PatientID);
    void medicationNotTaken(String patientID, String medication);
    void medicationTaken(String patientID, String medication);
}
