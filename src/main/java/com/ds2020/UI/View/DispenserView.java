package com.ds2020.UI.View;

import com.ds2020.RPCMethods;
import com.ds2020.UI.Controller.DispenserController;
import com.ds2020.UI.Util.JTableButtonModel;
import com.ds2020.UI.Util.JTableButtonRenderer;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import java.awt.*;

public class DispenserView extends JFrame implements IDispenserView {

    private final JPanel panelMain=new JPanel(new BorderLayout());
    private JPanel panelInformation=new JPanel(new GridLayout(0,2));
    private final JLabel dispenserMedicationsLabel=new JLabel("Dispenser Medications");
    private final JLabel timeLabel=new JLabel("23:25",SwingConstants.RIGHT);

    private JTable medicationJTable=new JTable();
    private JScrollPane spMedicationsLabel;

    public DispenserView(RPCMethods service){

        DispenserController dispenserController = new DispenserController(this, service);

        dispenserMedicationsLabel.setFont(new Font("Serif", Font.BOLD, 30));
        timeLabel.setFont(new Font("Serif", Font.PLAIN, 35));

        panelInformation.add(dispenserMedicationsLabel);
        panelInformation.add(timeLabel);

        spMedicationsLabel = new JScrollPane(medicationJTable);

        panelMain.add(panelInformation, BorderLayout.BEFORE_FIRST_LINE);
        panelMain.add(spMedicationsLabel, BorderLayout.CENTER);
        panelMain.setBorder(new EmptyBorder(10, 10, 10, 10));


        medicationJTable.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                dispenserController.valueTableSelected();
            }
        });

        this.add(panelMain);
        setMinimumSize(new Dimension(1200,800));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Dispenser");
        pack();
        setVisible(true);

    }


    @Override
    public void showTakePillMessage() {

    }

    @Override
    public void showForgotPillTakeMessage() {

    }

    @Override
    public void showMedicationTable(JTableButtonModel model) {

        TableCellRenderer tableRenderer;
        medicationJTable.setModel(model);
        tableRenderer = medicationJTable.getDefaultRenderer(JButton.class);
        medicationJTable.setDefaultRenderer(JButton.class, new JTableButtonRenderer(tableRenderer));
        //spMedicationsLabel = new JScrollPane(medicationJTable);
        //panelMain.add(spMedicationsLabel, BorderLayout.CENTER);

        medicationJTable.setFont(new Font("Arial", Font.BOLD, 20));
        medicationJTable.setRowHeight(80);

        JTableHeader tableHeader = medicationJTable.getTableHeader();
        tableHeader.setBackground(Color.black);
        tableHeader.setForeground(Color.white);
        Font headerFont = new Font("Verdana", Font.PLAIN, 20);
        tableHeader.setFont(headerFont);
        //medicationJTable.getColumnModel().getColumn(7).setPreferredWidth(150);
        panelMain.revalidate();
        panelMain.repaint();
    }

    @Override
    public void showCurrentTime(String time) {
        timeLabel.setText(time);
    }

    @Override
    public int getSelectedRow() {
        return medicationJTable.getSelectedRow();
    }

    @Override
    public int getSelectedColumn() {
        return medicationJTable.getSelectedColumn();
    }
}