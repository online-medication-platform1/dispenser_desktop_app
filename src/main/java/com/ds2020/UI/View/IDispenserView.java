package com.ds2020.UI.View;

import com.ds2020.UI.Util.JTableButtonModel;

import javax.swing.table.DefaultTableModel;

interface IDispenserDataProvider {

    int getSelectedRow();
    int getSelectedColumn();

}

interface IDispenserProvider {

    void showTakePillMessage();
    void showForgotPillTakeMessage();
    void showMedicationTable(JTableButtonModel model);
    void showCurrentTime(String time);

}

public interface IDispenserView extends IDispenserDataProvider, IDispenserProvider {
}

