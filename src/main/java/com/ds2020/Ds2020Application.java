package com.ds2020;

import com.ds2020.UI.View.DispenserView;
import com.ds2020.rmi.models.MedicationSimpleClass;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import javax.swing.*;

@SpringBootApplication
public class Ds2020Application {

	public static void main(String[] args) {

		SpringApplication.run(Ds2020Application.class, args);
		System.setProperty("java.awt.headless", "false");
		AnnotationConfigApplicationContext context =
				new AnnotationConfigApplicationContext(SpringContextConfig.class);
		RPCMethods service = context.getBean(RPCMethods.class);

		SwingUtilities.invokeLater(() -> {
			DispenserView dispenserView = new DispenserView(service);
			dispenserView.setVisible(true);
		});
	}

}
