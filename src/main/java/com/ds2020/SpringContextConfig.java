package com.ds2020;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.httpinvoker.HttpInvokerProxyFactoryBean;


@Configuration
@ComponentScan("com.ds2020.*")
public class SpringContextConfig {

    @Bean
    public HttpInvokerProxyFactoryBean invoker() {
        HttpInvokerProxyFactoryBean invoker = new HttpInvokerProxyFactoryBean();
        //invoker.setServiceUrl("http://localhost:8080/dispenser");
        invoker.setServiceUrl("https://ds2020-lucian-spring1.herokuapp.com/dispenser");
        invoker.setServiceInterface(RPCMethods.class);
        return invoker;
    }
}
